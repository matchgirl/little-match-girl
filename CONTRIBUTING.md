Hacking on this project
=======================

Wow, someone finally looked at the repo! Here are a few guidelines for how I 
think we should be planning to structure this. If you think any of these 
guidelines should be changed, raise a proposal on one of our team chats.

Branches
--------

Because Unity doesn't allow for easy merging when both scripts and their 
attached objects have changed, the programming team has requested to work in a 
branch downstream of the art branches. Thus, we've come up with a pipeline in 
the order of `art-dev` &#8594; `art` &#8594; `programming` &#8594; `master`. To 
explain the function of each branch:

- **Art-dev** is a place for the art team to push preliminary and even broken 
models or arrangements. As development progresses, this may become an 
unnecessary branch as we can mostly focusing on pushing small changes to the 
main art branch.
- **Art** is where scenes that are ready to be worked on by programming are 
merged. Programming will be pulling from this, so make sure that they have been 
notified of important changes so they can manually resolve any tricky conflicts.
- **Programming** is fairly straightforward; it's the place where scripts are 
written to work with the models merged from art, and is eventually merged to 
master when it's ready for scrutiny.
- **Master** should hold mostly working and presentable code, but should be by 
no means considered stable. Master is heavily protected.

### A word about scenes

Due to aforementioned issues with merging models with code, the art team will 
actually be working on **separate Unity scenes** in the art and art-dev 
branches. These scenes, ending in "-staging," will be a working place for 
environment design and placing models. The programming team will be periodically 
copying elements from here into the main scenes. It is certainly possible that 
they miss something, however, so everyone should be carefully watching the 
master branch for discrepancies.

Versioning
----------

Whenever the work in master has reached a fairly stable and desirable point, a 
tag should be created, conforming to the guidelines of 
[semantic versioning][SEMVER]. That is to say, the version numbers follow the 
format of `major.minor.patch`. Ideally, we should be able to identify a version 
1.0.0 shortly after the post-production phase.

It is also important that we label our commits informatively. Not only does this 
notify downstream developers of new changes, but it allows us to easily write up 
a [changelog][CHANGELOG] to put in the message of each new git tag. 

Bug reporting
-------------

Quite simple. If you believe there is a bug&#8212;that is, an unexpected 
behavior&#8212;in the master branch, file a report in the issue tracker with 
specific instructions on how to reproduce it. For now, we want to refrain from 
creating bugs for issues that simply haven't been implemented yet. However, 
after our first major release, the issue tracker will be open to feature 
requests.

Contributors with the maintainer access rank or higher should check if they can 
reproduce the bug, and if they can, assign to said bug a reasonable milestone. 
Assigning bugs to developers can be a useful way to mark a bug as yours to fix, 
or better yet, to shame someone inactive into working.

Licensing
---------

I've chosen the Mozilla Public License for now, as it is a weak copyleft license 
that will both permit people to use this code later on but not to redistribute 
what is essentially the same code with a proprietary license. According to 
Mozilla, the way to be sure our code is properly licensed is below:

>>>
The license notice must be in some way "attached" to each file. (Sec. 1.4.) In 
cases where putting it in the file is impossible or impractical, that 
requirement can be fulfilled by putting the notice somewhere that a recipient 
"would be likely to look for such a notice," such as a LICENSE file in the same 
directory as the file. (Ex. A.) The license notice is as short as possible 
(3 lines) to make it easy to put in as many different types of files as 
possible.

While the license permits putting the header somewhere other than the file 
itself, individual files often end up being distributed on their own, without 
the rest of the software they were authored with. As a result, putting the 
license notice in the file is the surest way to ensure that recipients are 
always notified.

For your convenience, Mozilla has produced [boilerplate headers][HEADERS] 
formatted with several common 'comment' syntaxes, suitable for copying and 
pasting.
>>>

Documentation
-------------

Document your code whenever possible, through inline comments. When something 
warrants a longer documentation (like this file), use create a markdown file and 
make sure there is no text beyond the eightieth text character of the file, 
excluding URLs in reference-style links. If you don't know Markdown, use 
GitLab's [guide][MDGUIDE].

Code style
----------

This is something the programming team has to decide on, but it is probably a 
good idea to end your files with one blank line, and mimic the style of the code 
around you.

About Google Drive
------------------

Since this is project is for IB's CAS program, everyone should be keeping 
meticulously **detailed logs** of their contributions, at least in the 
time-tracking sheet. Otherwise, those who don't want to contribute via git 
patches or commits will have little proof that they did any work. If you wish 
to be more specific, you may create another sheet in the document and protect 
it so that only you and the owner may edit.  
Below is a recommended format for such a sheet. You may omit the last column.

| date      | elapsed time | short description | git commit hash(es)     |
|:---------:|:------------:|:------------------|:-----------------------:|
| YYYY-M-DD | HH:MM        | a sentence or two | XXXXXXXX, YYYYYYYY, ... |

Google Drive also hosts our deadline compendium, which we should probably 
follow.&#8239;.&#8239;.&#8239;.

[SEMVER]: https://semver.org/
[CHANGELOG]: https://keepachangelog.com/en/1.0.0/
[HEADERS]: https://www.mozilla.org/en-US/MPL/headers/
[MDGUIDE]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md

